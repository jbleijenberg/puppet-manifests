node default {

  if ($::operatingsystem == 'CentOS') {
    exec { 'update-os':
      command => '/usr/bin/yum upgrade -y'
    }
  } else {
    if ($::operatingsystem == 'Ubuntu') {
      exec { 'update-os':
        command => '/usr/bin/apt-get update && apt-get upgrade -y'
      }
    }
  }
  
  hiera_include('classes')

  if  defined(Class['httpd']) {
    create_resources(httpd::vhost, hiera('vhosts'))
  }

  if defined(Class['mariadb']) {
    create_resources(mariadb::database, hiera('databases'))
  }

  if hiera('firewall') {
    firewall { '100 allow access':
      dport   => hiera('firewall'),
      proto  => tcp,
      action => accept,
    }
  }

  if defined(Class['solr']) {
    create_resources(solr::core, hiera('solr-cores'))
  }
}